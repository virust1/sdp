package JAXB;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class JaxBMarshal {
	
	public static void main(String[] args) {
		try {
		  File file = new File("employee.xml");    
          JAXBContext jaxbContext = JAXBContext.newInstance(codebeautify.class);    
       
          Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();    
          codebeautify e=(codebeautify) jaxbUnmarshaller.unmarshal(file);    
          System.out.println(e);  
            
        } catch (JAXBException e) {e.printStackTrace(); }  
		
		
	}

}
