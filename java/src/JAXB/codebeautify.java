﻿package JAXB;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement  
public class codebeautify {
 REQ REQObject;


 // Getter Methods 

 public REQ getREQ() {
  return REQObject;
 }

 // Setter Methods 

 public void setREQ(REQ REQObject) {
  this.REQObject = REQObject;
 }
}
 class REQ {
 private String FEATURE;
 private String SOURCE;
 private String VERSION;
 private String LANGUAGE;
 HEADER HEADERObject;
 BODY BODYObject;


 // Getter Methods 

 public String getFEATURE() {
  return FEATURE;
 }

 public String getSOURCE() {
  return SOURCE;
 }

 public String getVERSION() {
  return VERSION;
 }

 public String getLANGUAGE() {
  return LANGUAGE;
 }

 public HEADER getHEADER() {
  return HEADERObject;
 }

 public BODY getBODY() {
  return BODYObject;
 }

 // Setter Methods 

 public void setFEATURE(String FEATURE) {
  this.FEATURE = FEATURE;
 }

 public void setSOURCE(String SOURCE) {
  this.SOURCE = SOURCE;
 }

 public void setVERSION(String VERSION) {
  this.VERSION = VERSION;
 }

 public void setLANGUAGE(String LANGUAGE) {
  this.LANGUAGE = LANGUAGE;
 }

 public void setHEADER(HEADER HEADERObject) {
  this.HEADERObject = HEADERObject;
 }

 public void setBODY(BODY BODYObject) {
  this.BODYObject = BODYObject;
 }
}
class BODY {
 BULKCREATESUBSCRIPTION BULKCREATESUBSCRIPTIONObject;


 // Getter Methods 

 public BULKCREATESUBSCRIPTION getBULKCREATESUBSCRIPTION() {
  return BULKCREATESUBSCRIPTIONObject;
 }

 // Setter Methods 

 public void setBULKCREATESUBSCRIPTION(BULKCREATESUBSCRIPTION BULKCREATESUBSCRIPTIONObject) {
  this.BULKCREATESUBSCRIPTIONObject = BULKCREATESUBSCRIPTIONObject;
 }
}
class BULKCREATESUBSCRIPTION {
 OFFER OFFERObject;


 // Getter Methods 

 public OFFER getOFFER() {
  return OFFERObject;
 }

 // Setter Methods 

 public void setOFFER(OFFER OFFERObject) {
  this.OFFERObject = OFFERObject;
 }
}
 class OFFER {
 private String CODE;
 private String PRICEPLANSET;


 // Getter Methods 

 public String getCODE() {
  return CODE;
 }

 public String getPRICEPLANSET() {
  return PRICEPLANSET;
 }

 // Setter Methods 

 public void setCODE(String CODE) {
  this.CODE = CODE;
 }

 public void setPRICEPLANSET(String PRICEPLANSET) {
  this.PRICEPLANSET = PRICEPLANSET;
 }
}
 class HEADER {
 LOGIN LOGINObject;


 // Getter Methods 

 public LOGIN getLOGIN() {
  return LOGINObject;
 }

 // Setter Methods 

 public void setLOGIN(LOGIN LOGINObject) {
  this.LOGINObject = LOGINObject;
 }
}
 class LOGIN {
 private String USERNAME;
 private String PASSWORD;


 // Getter Methods 

 public String getUSERNAME() {
  return USERNAME;
 }

 public String getPASSWORD() {
  return PASSWORD;
 }

 // Setter Methods 

 public void setUSERNAME(String USERNAME) {
  this.USERNAME = USERNAME;
 }

 public void setPASSWORD(String PASSWORD) {
  this.PASSWORD = PASSWORD;
 }
}