package com.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReadingExample {
	
	
	public static void main(String[] args) throws IOException {
		
		String fileName= "A.txt";
		List<Object> fileData= new ArrayList();
		File f1= new File("A.txt");
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			
//			fileData= stream.collect(Collectors.toList());
//		    stream.forEach(System.out::println);
		    fileData= stream.collect(Collectors.toList());
		}
		System.out.println(fileData);
//		System.out.println(fileData.get(0));
		LocalDate futureDate = LocalDate.now();
		futureDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		DateTimeFormatter d1= DateTimeFormatter.ofPattern("dd/MM/yyyy");
		d1.format(futureDate);
		
		System.out.println(	d1.format(futureDate.plusMonths(6)));
//		System.out.println(futureDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
	}

}
