package com.test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class JDK8LIst {

		
		public static void main(String[] args){ 
	           
	         // list to save stream of strings 
	         List<String> arr = new ArrayList<>(); 
	           
	         arr.add("geeks"); 
	         arr.add("for"); 
	         arr.add("geeks"); 
	         arr.add("computer"); 
	         arr.add("science"); 
	          
	         Stream<String> str = arr.stream(); 
	           str.limit(2).forEach(System.out::println);
	     
	 
	}
}
