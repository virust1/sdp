package com.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.test.REQ.BODY;
import com.test.REQ.BODY.BULKSENDER;
import com.test.REQ.BODY.BULKSENDER.OFFER;
import com.test.REQ.BODY.BULKSHORTCODE;
import com.test.REQ.HEADER;
import com.test.REQ.HEADER.LOGIN;

public class JaxBMarshal {
	
	public static void main(String[] args) throws FileNotFoundException {
		try {
//		  File file = new File("employee.xml");    
//          JAXBContext jaxbContext = JAXBContext.newInstance(REQ.class);    
//       
//          Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();    
//          REQ e=(REQ) jaxbUnmarshaller.unmarshal(file);    
//          System.out.println(e.getOPERATIONCODE());  
			
			String s="Bu1lk";
			
			JAXBContext contextObj = JAXBContext.newInstance(REQ.class);  
			  
		    Marshaller marshallerObj = contextObj.createMarshaller();  
		    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
		    REQ r1= new REQ();
		    r1.setFEATURE("fsds");
		    r1.setTIMESTAMP("svdfd");
		    r1.setRESPONSEURL("sdfd");
		    r1.setOPERATIONCODE("ewrer");
		    r1.setCHANNELID("dfdf");
		    r1.setCLIENTTRANSID("dfdf");
		    r1.setSOURCE("gfgfg");
		    r1.setVERSION("dsdfdf");
		    r1.setLANGUAGE("dfdfdf");
		    
		    LOGIN l1= new LOGIN();
		    l1.setUSERNAME("sdgdf");
		    l1.setPASSWORD("dffddf");
		    HEADER h1= new HEADER();
		    h1.setLOGIN(l1);
		    
		    r1.setHEADER(h1);
		    
		    OFFER o1= new OFFER();
		    o1.setCODE("ddfdf");
		    o1.setPRICEPLANSET("dsvdfdf");
		    BODY b1= new BODY();  
		    if(s.equals("Bulk")){
		    	BULKSHORTCODE bk= new BULKSHORTCODE();
			    bk.setOFFER(o1);
			    bk.setACTUALFILENAME("dsfdfdf");
			    bk.setCIRCLECODE("fgjflgkfjgkljfglkjf");
			    bk.setFILENAME("dfdfdfdf");
			    bk.setSERVICECLASS("dfdfdf");
			    bk.setType("Promotion");
			    b1.setBulkShortCode(bk);
		    }else {
		    	BULKSENDER bk= new BULKSENDER();
			    bk.setOFFER(o1);
			    bk.setACTUALFILENAME("dsfdfdf");
			    bk.setCIRCLECODE("ddfdfdfd");
			    bk.setFILENAME("dfdfdfdf");
			    bk.setSERVICECLASS("dfdfdf");	
			    b1.setBulksender(bk);
		    }
		    
		  
		    r1.setBODY(b1);
		  
		      
		   marshallerObj.marshal(r1, new FileOutputStream("request.xml"));  
		   
		   StringWriter sw = new StringWriter();
           
           //Write XML to StringWriter
		   marshallerObj.marshal(r1, sw);
            
           //Verify XML Content
           String xmlContent = sw.toString();
           System.out.println( xmlContent );
            
        } catch (JAXBException e) {e.printStackTrace(); }  
		
		
	}

}
