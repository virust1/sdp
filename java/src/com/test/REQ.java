package com.test;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "feature",
    "timestamp",
    "responseurl",
    "operationcode",
    "channelid",
    "clienttransid",
    "source",
    "version",
    "language",
    "header",
    "body"
})
@XmlRootElement(name = "REQ")
public class REQ {

    @XmlElement(name = "FEATURE", required = true)
    protected String feature;
    @XmlElement(name = "TIME-STAMP", required = true)
    protected String timestamp;
    @XmlElement(name = "RESPONSE-URL", required = true)
    protected String responseurl;
    @XmlElement(name = "OPERATION-CODE", required = true)
    protected String operationcode;
    @XmlElement(name = "CHANNEL-ID", required = true)
    protected String channelid;
    @XmlElement(name = "CLIENT-TRANS-ID", required = true)
    protected String clienttransid;
    @XmlElement(name = "SOURCE", required = true)
    protected String source;
    @XmlElement(name = "VERSION", required = true)
    protected String version;
    @XmlElement(name = "LANGUAGE", required = true)
    protected String language;
    @XmlElement(name = "HEADER", required = true)
    protected REQ.HEADER header;
    @XmlElement(name = "BODY", required = true)
    protected REQ.BODY body;

    public String getFEATURE() {
        return feature;
    }

    public void setFEATURE(String value) {
        this.feature = value;
    }
    public String getTIMESTAMP() {
        return timestamp;
    }

  
    public void setTIMESTAMP(String value) {
        this.timestamp = value;
    }

    
    public String getRESPONSEURL() {
        return responseurl;
    }

    
    public void setRESPONSEURL(String value) {
        this.responseurl = value;
    }

   
    public String getOPERATIONCODE() {
        return operationcode;
    }
    
    public void setOPERATIONCODE(String value) {
        this.operationcode = value;
    }

    /**
     * Gets the value of the channelid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHANNELID() {
        return channelid;
    }

    /**
     * Sets the value of the channelid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHANNELID(String value) {
        this.channelid = value;
    }

    /**
     * Gets the value of the clienttransid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLIENTTRANSID() {
        return clienttransid;
    }

    /**
     * Sets the value of the clienttransid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLIENTTRANSID(String value) {
        this.clienttransid = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOURCE() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOURCE(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVERSION() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVERSION(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUAGE() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUAGE(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link REQ.HEADER }
     *     
     */
    public REQ.HEADER getHEADER() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link REQ.HEADER }
     *     
     */
    public void setHEADER(REQ.HEADER value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link REQ.BODY }
     *     
     */
    public REQ.BODY getBODY() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link REQ.BODY }
     *     
     */
    public void setBODY(REQ.BODY value) {
        this.body = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BULKCREATESUBSCRIPTION">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="OFFER">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PRICEPLANSET" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="SERVICE-CLASS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CIRCLE-CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FILE-NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ACTUAL-FILENAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bulksender",
    		"bulkShortCode"
    })
    public static class BODY {
    	
        @XmlElement(name = "BULKSENDER", required = false)
        protected REQ.BODY.BULKSENDER bulksender;
        
        @XmlElement(name = "BULKSHORTCODE", required = false)
        protected REQ.BODY.BULKSHORTCODE bulkShortCode;
        public REQ.BODY.BULKSENDER getBulksender() {
			return bulksender;
		}

		public void setBulksender(REQ.BODY.BULKSENDER bulksender) {
			this.bulksender = bulksender;
		}

		public REQ.BODY.BULKSHORTCODE getBulkShortCode() {
			return bulkShortCode;
		}

		public void setBulkShortCode(REQ.BODY.BULKSHORTCODE bulkShortCode) {
			this.bulkShortCode = bulkShortCode;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "offer",
            "serviceclass",
            "circlecode",
            "filename",
            "actualfilename",
            "entityName",
            "type",
            "id"
        })
        public static class BULKSENDER {

            @XmlElement(name = "OFFER", required = true)
            protected REQ.BODY.BULKSENDER.OFFER offer;
            @XmlElement(name = "SERVICE-CLASS", required = true)
            protected String serviceclass;
            @XmlElement(name = "CIRCLE-CODE", required = true)
            protected String circlecode;
            @XmlElement(name = "FILE-NAME", required = true)
            protected String filename;
            @XmlElement(name = "ACTUAL-FILENAME", required = true)
            protected String actualfilename;
            @XmlElement(name = "ENTITY_NAME", required = true)
            protected String entityName;
            @XmlElement(name = "TYPE", required = true)
            protected String type;
            @XmlElement(name = "ID", required = true)
            protected String id;
            /**
             * Gets the value of the offer property.
             * 
             * @return
             *     possible object is
             *     {@link REQ.BODY.BULKCREATESUBSCRIPTION.OFFER }
             *     
             */
            public REQ.BODY.BULKSENDER.OFFER getOFFER() {
                return offer;
            }
            
            

            public String getEntityName() {
				return entityName;
			}



			public void setEntityName(String entityName) {
				this.entityName = entityName;
			}



			public String getType() {
				return type;
			}



			public void setType(String type) {
				this.type = type;
			}



			public String getId() {
				return id;
			}



			public void setId(String id) {
				this.id = id;
			}



			/**
             * Sets the value of the offer property.
             * 
             * @param value
             *     allowed object is
             *     {@link REQ.BODY.BULKCREATESUBSCRIPTION.OFFER }
             *     
             */
            public void setOFFER(REQ.BODY.BULKSENDER.OFFER value) {
                this.offer = value;
            }

            /**
             * Gets the value of the serviceclass property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSERVICECLASS() {
                return serviceclass;
            }

            /**
             * Sets the value of the serviceclass property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSERVICECLASS(String value) {
                this.serviceclass = value;
            }

            /**
             * Gets the value of the circlecode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCIRCLECODE() {
                return circlecode;
            }
            
            

            /**
             * Sets the value of the circlecode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCIRCLECODE(String value) {
                this.circlecode = value;
            }

            /**
             * Gets the value of the filename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFILENAME() {
                return filename;
            }

           
            public void setFILENAME(String value) {
                this.filename = value;
            }

           
            public String getACTUALFILENAME() {
                return actualfilename;
            }

          
            public void setACTUALFILENAME(String value) {
                this.actualfilename = value;
            }


          
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "code",
                "priceplanset"
            })
            public static class OFFER {

                @XmlElement(name = "CODE", required = true)
                protected String code;
                @XmlElement(name = "PRICEPLANSET", required = true)
                protected String priceplanset;

                /**
                 * Gets the value of the code property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCODE() {
                    return code;
                }

                /**
                 * Sets the value of the code property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCODE(String value) {
                    this.code = value;
                }

                /**
                 * Gets the value of the priceplanset property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPRICEPLANSET() {
                    return priceplanset;
                }

                /**
                 * Sets the value of the priceplanset property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPRICEPLANSET(String value) {
                    this.priceplanset = value;
                }

            }

        }
        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
           
        })
        public static class BULKSHORTCODE extends BULKSENDER {
        	
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LOGIN">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="USERNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PASSWORD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "login"
    })
    public static class HEADER {

        @XmlElement(name = "LOGIN", required = true)
        protected REQ.HEADER.LOGIN login;

        /**
         * Gets the value of the login property.
         * 
         * @return
         *     possible object is
         *     {@link REQ.HEADER.LOGIN }
         *     
         */
        public REQ.HEADER.LOGIN getLOGIN() {
            return login;
        }

        /**
         * Sets the value of the login property.
         * 
         * @param value
         *     allowed object is
         *     {@link REQ.HEADER.LOGIN }
         *     
         */
        public void setLOGIN(REQ.HEADER.LOGIN value) {
            this.login = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="USERNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PASSWORD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "username",
            "password"
        })
        public static class LOGIN {

            @XmlElement(name = "USERNAME", required = true)
            protected String username;
            @XmlElement(name = "PASSWORD", required = true)
            protected String password;

            /**
             * Gets the value of the username property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUSERNAME() {
                return username;
            }

            /**
             * Sets the value of the username property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUSERNAME(String value) {
                this.username = value;
            }

            /**
             * Gets the value of the password property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPASSWORD() {
                return password;
            }

            /**
             * Sets the value of the password property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPASSWORD(String value) {
                this.password = value;
            }

        }

    }

}
