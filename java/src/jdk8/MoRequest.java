package jdk8;

public class MoRequest {
	Request request;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "MoRequest [request=" + request + "]";
	}
	
	
}
