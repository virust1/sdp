package jdk8;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadFile {
	
	public static void main(String[] args) throws IOException {
File f1= new File("A.properties");
FileInputStream fin= new FileInputStream(f1);

Properties p1= new Properties();
p1.load(fin);
System.out.println(p1.getProperty("name"));
	}
}
