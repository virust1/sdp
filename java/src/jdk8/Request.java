package jdk8;

import java.util.ArrayList;

public class Request {
	 private String request_id;
	 private String action;
	 private String request_timestamp;
	 private String source_system;
	 private String user_name;
	 private String status;
	 ArrayList < service_configuration > service_configuration = new ArrayList < service_configuration > ();

	 public String getRequest_id() {
	  return request_id;
	 }

	 public String getAction() {
	  return action;
	 }

	 public String getRequest_timestamp() {
	  return request_timestamp;
	 }

	 public String getSource_system() {
	  return source_system;
	 }

	 public String getUser_name() {
	  return user_name;
	 }

	 public String getStatus() {
	  return status;
	 }

	 // Setter Methods 

	 public void setRequest_id(String request_id) {
	  this.request_id = request_id;
	 }

	 public void setAction(String action) {
	  this.action = action;
	 }

	 public void setRequest_timestamp(String request_timestamp) {
	  this.request_timestamp = request_timestamp;
	 }

	 public void setSource_system(String source_system) {
	  this.source_system = source_system;
	 }

	 public void setUser_name(String user_name) {
	  this.user_name = user_name;
	 }

	 public void setStatus(String status) {
	  this.status = status;
	 }

	public ArrayList<service_configuration> getService_configuration() {
		return service_configuration;
	}

	public void setService_configuration(ArrayList<service_configuration> service_configuration) {
		this.service_configuration = service_configuration;
	}

	@Override
	public String toString() {
		return "Request [request_id=" + request_id + ", action=" + action + ", request_timestamp=" + request_timestamp
				+ ", source_system=" + source_system + ", user_name=" + user_name + ", status=" + status
				+ ", service_configuration=" + service_configuration + "]";
	}
	 
	 
}
