package jdk8;

public class service_configuration {
	
	private String keyword;
	private String offercode;
	private String oprationCode;
	private String shortcode;
	private String cpId;
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getOffercode() {
		return offercode;
	}
	public void setOffercode(String offercode) {
		this.offercode = offercode;
	}
	public String getOprationCode() {
		return oprationCode;
	}
	public void setOprationCode(String oprationCode) {
		this.oprationCode = oprationCode;
	}
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	@Override
	public String toString() {
		return "service_configuration [keyword=" + keyword + ", offercode=" + offercode + ", oprationCode="
				+ oprationCode + ", shortcode=" + shortcode + ", cpId=" + cpId + "]";
	}
	
	
}
