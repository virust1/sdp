package maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class fileProcress {
	public static void main(String[] args) throws IOException {
		 FileReader reader=new FileReader("ftp.properties");  
	      
		    Properties p=new Properties();  
		    p.load(reader);  
		    String instance= p.getProperty("instance");
		    
		    for(int i =0; i<Integer.parseInt(instance);i++) {
		    	 String server = p.getProperty("ftp.host_"+i);
		         String port =p.getProperty("ftp.port_"+i);
		         String user = p.getProperty("ftp.username_"+i);
		         String pass = p.getProperty("ftp.password_"+i);
		         String path=p.getProperty("ftp.location_"+i);
		         FTPClient ftpClient = new FTPClient();
		         process(ftpClient,server,Integer.parseInt(port),user,pass,path);
		    }
    }

        
        
        
	
	
	private static void process(FTPClient ftpClient, String server, int port, String user, String pass,
			String remotePath) {
		   try {
	            ftpClient.connect(server);
	            ftpClient.login(user, pass);
	            ftpClient.enterLocalPassiveMode();
	            System.out.println("Connected");
	            String localDirPath = "D:/ftp";
	            uploadDirectory(ftpClient, remotePath, localDirPath, "");
	            ftpClient.logout();
	            ftpClient.disconnect();
	            System.out.println("Disconnected");
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	}

	public static boolean uploadSingleFile(FTPClient ftpClient,
	        String localFilePath, String remoteFilePath) throws IOException {
	    File localFile = new File(localFilePath);
	 
	    InputStream inputStream = new FileInputStream(localFile);
	    try {
	        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	        return ftpClient.storeFile(remoteFilePath, inputStream);
	    } finally {
	        inputStream.close();
	    }
	}
	public static void uploadDirectory(FTPClient ftpClient,
	        String remoteDirPath, String localParentDir, String remoteParentDir)
	        throws IOException {
	 
	    System.out.println("LISTING directory: " + localParentDir);
	 
	    File localDir = new File(localParentDir);
	    File[] subFiles = localDir.listFiles();
	    if (subFiles != null && subFiles.length > 0) {
	        for (File item : subFiles) {
	            String remoteFilePath = remoteDirPath + "/" + remoteParentDir
	                    + "/" + item.getName();
	            if (remoteParentDir.equals("")) {
	                remoteFilePath = remoteDirPath + "/" + item.getName();
	            }
	 
	 
	            if (item.isFile()) {
	                // upload the file
	                String localFilePath = item.getAbsolutePath();
	                System.out.println("About to upload the file: " + localFilePath);
	                boolean uploaded = uploadSingleFile(ftpClient,
	                        localFilePath, remoteFilePath);
	                if (uploaded) {
	                    System.out.println("UPLOADED a file to: "
	                            + remoteFilePath);
	                } else {
	                    System.out.println("COULD NOT upload the file: "
	                            + localFilePath);
	                }
	            } else {
	                // create directory on the server
	                boolean created = ftpClient.makeDirectory(remoteFilePath);
	                if (created) {
	                    System.out.println("CREATED the directory: "
	                            + remoteFilePath);
	                } else {
	                    System.out.println("COULD NOT create the directory: "
	                            + remoteFilePath);
	                }
	 
	                // upload the sub directory
	                String parent = remoteParentDir + "/" + item.getName();
	                if (remoteParentDir.equals("")) {
	                    parent = item.getName();
	                }
	 
	                localParentDir = item.getAbsolutePath();
	                uploadDirectory(ftpClient, remoteDirPath, localParentDir,
	                        parent);
	            }
	        }
	    }
	}

 
}
