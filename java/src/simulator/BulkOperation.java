package simulator;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BulkOperation {
	
	public static void main(String[] args) {
		try{
			
		 FileReader reader=new FileReader("ftp.properties");  
		 List<Object> fileData= new ArrayList<Object>();
		 String table_Name="CP_SENDER_NAME";
		    Properties p=new Properties();  
		    p.load(reader);
		    File folder = new File(p.getProperty("directory"));
		    File[] listOfFiles = folder.listFiles();
		   
		    for (File file : listOfFiles) {
		    	try (Stream<String> stream = Files.lines(Paths.get(file.getName()))) {
					fileData= stream.collect(Collectors.toList());
				}catch (Exception e) {
					e.printStackTrace();
				}	
		    }
		    //jdbc:mysql://10.0.0.18:3306/SAFARICOM
		    String URL= p.getProperty("URL");
		    //scmuser
		    String UserName= p.getProperty("UserName"); 
		    String password= p.getProperty("password"); 
		    Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(URL, UserName, password);
		    
		    if(fileData.size()>0) {
				Iterator<Object> iterator= fileData.iterator();
				while(iterator.hasNext()) {
					String dBValue = iterator.next().toString();
					PreparedStatement pstmt = null;
					String sql = null;
					boolean status=true;
						try {
							sql = " insert into "+table_Name+"(NAME,DESCRIPTION,"
									+ " PARENT_ID,START_DATE,END_DATE,STATUS,TYPE,"
									+ " CP_NAME)"
									+ " values(?,?,?,?,?,?,?,?)";

							pstmt = conn.prepareStatement(sql);
							pstmt.setString(1, dBValue);
							pstmt.setString(2, null);
							pstmt.setString(3, bulkInformationDTO.getParentId());
							pstmt.setString(4, bulkInformationDTO.getStartDate());
							pstmt.setString(5, bulkInformationDTO.getEndDate());
							pstmt.setString(6, bulkInformationDTO.getStatus());
							pstmt.setString(7, bulkInformationDTO.getType());
							pstmt.setString(8, bulkInformationDTO.getCpName());
							int x=pstmt.executeUpdate();
						} 
					
				}
		    }
		    
		    
		    
		}catch (Exception e) {
			// TODO: handle exception
		}        
			
	}

}
