package com.6DTest.password;

public interface PasswordService {

    String hash(String input);

    String algorithm();

}